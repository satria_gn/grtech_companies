
$(document).ready(function(){
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
        theme: 'bootstrap4'
    })

    $("#tb_company").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#tb_company_wrapper .col-md-6:eq(0)');

});

function ResetForm() {
    $('#form-create')[0].reset();
}

function Modalcreate()
{
    $("#create-Items").modal('show');
}

function closeCreate(){
    $("#form-create")[0].reset();
}

function closeEdit(){
    $("#form-edit")[0].reset();
}

function editcompany(id) {
    $.ajax({
        type: "GET",
        url: "/company/show_edit/" + id,
        success: function(data) {
            $("#edit-Items").modal('show');
            $('.select23').select2(
                {
                    dropdownParent: $('#pilih'),
                });

            $("#form-edit input[name=id]").val(data.detail.id);
            $("#form-edit input[name=name]").val(data.detail.name);
            $("#form-edit input[name=email]").val(data.detail.email);
            // $("#form-edit input[name=logo]").val(data.detail.logo);
            $("#form-edit input[name=website]").val(data.detail.website);
        }
    });
}
$('#form-edit').on('submit', function(e){
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: "POST",
        url: "/company/update/"+$("#form-edit input[name=id]").val(),
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        dataType: "json",
        beforeSend: function(){
            $('.btn_update').addClass('btn-progress');
        },
        success: function(response) {
            if (response.success) {
                $('.btn_update').removeClass('btn-progress');
                $('#form-edit')[0].reset();
                $("#edit-Items").modal('hide');
                setTimeout(function() {
                    location.reload();
                }, 1000);
                toastr.success(response.success);
            }
        }
    });
});

function deletecompany(id) {
    $.ajax({
        type: "GET",
        url: "/company/show_delete/" + id,
        success: function(data) {
            $("#modal-delete").modal('show');
            $("#delete_company #delete-title").html("Delete (" + data.detail.name + ")?");
            $("#delete_company input[name=id]").val(data.detail.id);
        }
    });
}

$("#btn-delete").click(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'DELETE',
        url: '/company/' + $("#delete_company input[name=id]").val() + '/delete',
        dataType: 'json',
        beforeSend: function(){
            $('#btn-delete').addClass('btn-progress');
        },

        success: function(response) {
            if (response.success) {
                $('#btn-delete').removeClass('btn-progress');
                $("#modal-delete").modal('hide');
                setTimeout(function() {
                    location.reload();
                }, 1000);
                toastr.success(response.success);
            }
        }
    });
});
