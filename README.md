## Step Installasi Test Laravel PT.GRTECH

- setting file .env

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=companies_grtech
DB_USERNAME=root
DB_PASSWORD=

Run
- composer install
- php artisan key:generate
- php artisan migrate:fresh
- php artisan db:seed
- php artisan serve
## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
