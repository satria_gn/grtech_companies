<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
    <!-- Add icons to the links using the .nav-icon class
         with font-awesome or any other icon font library -->
    <li class="nav-item {{ request()->is('home') ? 'active' : '' }}">
        <a href="{{url('/home')}}" class="nav-link {{ request()->is('home') ? 'nav-link active' : '' }}">
            <i class="nav-icon fas fa-newspaper"></i>
            <p>
                Dashboard
            </p>
        </a>
    </li>
    @if (Auth::user()->role == 'ADMIN')
    <li class="nav-item {{ request()->is('company') ? 'active' : '' }}">
        <a href="{{url('/company')}}" class="nav-link {{ request()->is('company*') ? 'nav-link active' : '' }}">
            <i class="nav-icon fa fa-building"></i>
            <p>
                Company
            </p>
        </a>
    </li>
    <li class="nav-item">
        <a href="{{url('/employee')}}" class="nav-link {{ request()->is('employee*') ? 'nav-item active' : '' }}">
            <i class="nav-icon fas fa-user"></i>
            <p>
                Employee
            </p>
        </a>
    </li>
    @endif
</ul>
