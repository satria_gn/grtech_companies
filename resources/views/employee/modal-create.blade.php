<div class="modal fade" id="create-Items">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Create Employee</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @if (count($errors)>0)
            @foreach ($errors->all() as $error )
                <p class="alert alert-danger">{{ $error }}</p>
            @endforeach

            @endif
            <form id="form-create" method="POST" action="{{ route('employee.store') }}" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="col-md-8">
                        <div class="card-body">
                            <div class="form-group">
                                <label>First Name</label>
                                <input type="text" class="form-control form-control-sm" id="first_name" name="first_name" placeholder="" required>
                            </div>
                            <div class="form-group">
                                <label>Last Name</label>
                                <input type="text" class="form-control form-control-sm" id="last_name" name="last_name" placeholder="" required>
                            </div>
                            <div class="form-group">
                                <label>Companies</label>
                                <select name="companies_id" class="form-control select2" style="width: 100%; hight: 20%">
                                    <option selected="selected">-Chose Companies-</option>
                                    @foreach ($company_data as $item)
                                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" class="form-control form-control-sm" id="email" name="email" placeholder="" required>
                            </div>
                            <div class="form-group">
                                <label>Phone</label>
                                <input type="text" class="form-control form-control-sm" id="phone" name="phone" placeholder="" required>
                            </div>
                        </div>
                    </div>
                </div>
            <div class="modal-footer justify-content-between">
                <a data-dismiss="modal" class="btn btn-default btn-sm" onclick="closeCreate()" href="javascript:void(0);"> Close</a>
                <button type="submit" class="btn bg-gradient-primary btn-sm">Save</button>
            </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
