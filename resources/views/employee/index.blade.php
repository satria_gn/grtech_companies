@extends('layout.template')
@section('title','Employee')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Employee</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Icons</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div style="display:none" align="left" class="card-header">
                                <a type="button" class="btn btn-outline-dark btn-sm">Close</a>
                            </div>
                            <div align="right" class="card-header">
                                <button onclick="Modalcreate()" type="button" class="btn btn-success float-right"><i class=""></i>
                                    Create Employee
                                </button>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="tb_employee" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th style="width: 10px; text-align: center">No</th>
                                        <th>Full Name</th>
                                        <th>Companies</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <th style="width: 170px;text-align: center">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($employee_data as $item)
                                        <tr>
                                            <td class="text-center"> {{ $loop->iteration }} </td>
                                            <td><a href="{{url('employee/'.$item->id.'/detail')}}" >{{$item->full_name}}</a></td>
                                            <td><a onclick="showcompany('{{$item->companies_id}}')" href="#">{{$item->companies->name}}</a>
                                            <td>{{$item->email}}</td>
                                            <td>{{$item->phone}}</td>
                                            <td style="text-align: center">
                                                <a onclick="editemployee('{{$item->id}}')" class="btn btn-info btn-sm" href="#"><i class="fas fa-pencil-alt"></i> Edit</a>
                                                <a onclick="deleteemployee('{{$item->id}}')" class="btn btn-danger btn-sm" href="#"><i class="fas fa-trash"></i> Delete</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
     @include('employee.modal-create')
     @include('employee.modal-edit')
     @include('employee.modal-delete')
     @include('employee.modal-companies')
@endsection
@push('js')
    <script src="{{ asset('management/employee.js') }}"></script>
@endpush

