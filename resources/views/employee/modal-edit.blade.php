<div class="modal fade" id="edit-Items">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit Employee</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form id="form-edit">
                <div class="modal-body">
                    <div class="col-md-8">
                        <div class="card-body">
                            <div class="form-group">
                                <label>First Name</label>
                                <input type="text" class="form-control form-control-sm" id="first_name" name="first_name" >
                            </div>
                            <div class="form-group">
                                <label>Last Name</label>
                                <input type="text" class="form-control form-control-sm" id="last_name" name="last_name" >
                            </div>
                            <div id="pilih" class="form-group">
                                <label>Company</label>
                                <select name="companies_id" class="form-control select23 companies_id" style="width: 100%; hight: 20%">
                                    <option value="">-------</option>
                                    @foreach ($company_data as $item)
                                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" class="form-control form-control-sm" id="email" name="email" required>
                            </div>
                            <div class="form-group">
                                <label>Phone</label>
                                <input type="text" class="form-control form-control-sm" id="phone" name="phone" placeholder="" required>
                            </div>
                        </div>
                    </div>
                </div>
            <div class="modal-footer justify-content-between">
                <a data-dismiss="modal" class="btn btn-default btn-sm" onclick="closeEdit()" href="javascript:void(0);"> Close</a>
                <input type="hidden" name="id" readonly>
                <input type="submit" value="Update" class="btn bg-gradient-primary btn-sm btn_update">
            </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
