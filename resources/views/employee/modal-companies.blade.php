<div class="modal fade" id="show-company">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Detail Company</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form id="form-edit">
                <div class="modal-body">
                    <div class="col-md-8">
                        <div class="card-body">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" class="form-control form-control-sm" id="name" name="name" readonly>
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" class="form-control form-control-sm" id="email" name="email" placeholder="" readonly>
                            </div>
                            <div class="form-group">
                                <label>Website</label>
                                <input type="text" class="form-control form-control-sm" id="website" name="website" placeholder="" readonly>
                            </div>
                        </div>
                    </div>
                </div>
            <div class="modal-footer justify-content-between">
                <a data-dismiss="modal" class="btn btn-default btn-sm" href="javascript:void(0);"> Close</a>
            </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
