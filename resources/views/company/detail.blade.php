@extends('layout.template')
@section('title','Company')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Detail Company</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Icons</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">
                                    Company
                                </h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div style="background-color: #dfe6e9; width: 50%" class="form-group">
                                            <label>Name</label>
                                            <dd>{{$company->name}}</dd>
                                        </div>
                                        <div class="form-group">
                                            <label>Email</label>
                                            <dd>{{$company->email}}</dd>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Website</label>
                                            <dd>{{$company->website}}</dd>
                                        </div>
                                        <div class="form-group">
                                            <label>Logo</label>
                                            <dd><img style="width: 70px; height: 70px" class="profile-user-img"
                                                src="{{asset('img/company/'.$company->logo)}}"></dd>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <a href="{{url('company')}}" id="back" type="button" class="btn btn-secondary btn-sm"><i class="fas fa-arrow-left"></i> Back</a>
                                <a onclick="editcompany('{{$company->id}}')" class="btn btn-info btn-sm" href="#"><i class="fas fa-pencil-alt"></i> Edit</a>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- ./col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>

    @include('company.modal-edit')
@endsection
@push('js')
    <script src="{{ asset('management/company.js') }}"></script>
@endpush
