@extends('layout.template')
@section('title','Company')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Company</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Icons</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div style="display:none" align="left" class="card-header">
                                <a type="button" class="btn btn-outline-dark btn-sm">Close</a>
                            </div>
                            <div align="right" class="card-header">
                                <button onclick="Modalcreate()" type="button" class="btn btn-success float-right"><i class=""></i>
                                    Create Company
                                </button>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="tb_company" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th style="width: 10px; text-align: center">No</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Logo</th>
                                        <th>Website</th>
                                        <th style="width: 170px;text-align: center">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($company as $companies)
                                        <tr>
                                            <td class="text-center"> {{ $loop->iteration }} </td>
                                            <td><b><a href="{{url('company/'.$companies->id.'/detail')}}" >{{$companies->name}}</a></b></td>
                                            <td>{{$companies->email}}</td>
                                            <td style="width: 90px; height: 50px"><img style="width: 50px; height: 50px" class="profile-user-img"
                                                src="{{asset('img/company/'.$companies->logo)}}">
                                            </td>
                                            <td><a href="//{{$companies->website}}"><b>{{$companies->website}}</b></a></td>
                                            <td style="text-align: center">
                                                <a onclick="editcompany('{{$companies->id}}')" class="btn btn-info btn-sm" href="#"><i class="fas fa-pencil-alt"></i> Edit</a>
                                                <a onclick="deletecompany('{{$companies->id}}')" class="btn btn-danger btn-sm" href="#"><i class="fas fa-trash"></i> Delete</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
     @include('company.modal-create')
     @include('company.modal-edit')
     @include('company.modal-delete')
@endsection
@push('js')
    <script src="{{ asset('management/company.js') }}"></script>
@endpush

