<div class="modal fade" id="create-Items">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Create Company</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @if (count($errors)>0)
            @foreach ($errors->all() as $error )
                <p class="alert alert-danger">{{ $error }}</p>
            @endforeach

            @endif
            <form id="form-create" method="POST" action="{{ route('company.store') }}" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="col-md-8">
                        <div class="card-body">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" class="form-control form-control-sm" id="name" name="name" placeholder="" required>
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" class="form-control form-control-sm" id="email" name="email" placeholder="" required>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputFile">Logo</label>
                                <div class="input-group input-group-sm">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="logo" name="logo">
                                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Website</label>
                                <input type="text" class="form-control form-control-sm" id="website" name="website" placeholder="" required>
                            </div>
                        </div>
                    </div>
                </div>
            <div class="modal-footer justify-content-between">
                <a data-dismiss="modal" class="btn btn-default btn-sm" onclick="closeCreate()" href="javascript:void(0);"> Close</a>
                <button type="submit" class="btn bg-gradient-primary btn-sm">Save</button>
            </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
