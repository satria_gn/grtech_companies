<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::prefix('/company')->group(function() {
  Route::get('/', [App\Http\Controllers\CompanyController::class, 'index'])->name('company');
  Route::post('/store', [App\Http\Controllers\CompanyController::class, 'store'])->name('company.store');
  Route::get('/show_edit/{id}', [App\Http\Controllers\CompanyController::class, 'showedit'])->name('company.show_edit');
  Route::post('/update/{id}', [App\Http\Controllers\CompanyController::class, 'update']);
  Route::get('/show_delete/{id}',  [App\Http\Controllers\CompanyController::class, 'showdelete'])->name('company.show_delete');
  Route::delete('/{id}/delete', [App\Http\Controllers\CompanyController::class, 'destroy']);
  Route::get('/{id}/detail', [App\Http\Controllers\CompanyController::class, 'detail']);
});
// KONTRAK
Route::prefix('/employee')->group(function() {
  Route::get('/', [App\Http\Controllers\EmployeeController::class, 'index'])->name('employee');
  Route::post('/store', [App\Http\Controllers\EmployeeController::class, 'store'])->name('employee.store');
  Route::get('/show_edit/{id}', [App\Http\Controllers\EmployeeController::class, 'showedit'])->name('employee.show_edit');
  Route::post('/update/{id}', [App\Http\Controllers\EmployeeController::class, 'update']);
  Route::get('/show_delete/{id}',  [App\Http\Controllers\EmployeeController::class, 'showdelete'])->name('employee.show_delete');
  Route::delete('/{id}/delete', [App\Http\Controllers\EmployeeController::class, 'destroy']);
  Route::get('/{id}/detail', [App\Http\Controllers\EmployeeController::class, 'detail']);
});
