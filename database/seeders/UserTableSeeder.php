<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User;
        $user::updateOrCreate([
            'name'      => 'Administration',
            'email'     => 'admin@grtech.com.my',
            'password'  => bcrypt('password'),
            'role'      => 'ADMIN',
        ]);
        $user::updateOrCreate([
            'name'      => 'User',
            'email'     => 'user@grtech.com.my',
            'password'  => bcrypt('password'),
            'role'      => 'USER',
        ]);
    }
}
