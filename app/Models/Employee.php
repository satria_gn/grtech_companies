<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{

    protected $table = 'employees';
    protected $fillable = [
        'first_name', 'last_name','companies_id'
    ];

    public function getFullNameAttribute ()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function companies()
    {
        return $this->belongsTo(Company::class);
    }
}
