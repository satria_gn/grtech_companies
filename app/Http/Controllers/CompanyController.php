<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Company;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Image;
use File;

class CompanyController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $company = DB::table('companies')
            ->select('companies.*')
            ->Orderby('id','DESC')
            ->get();
        return view('company.index', compact('company'));
    }

    public function create()
    {
        return view('company.create');
    }


    public function deleteImage($file)
    {
        if (file_exists(public_path('img/company/'.$file))) {
            unlink(public_path('img/company/'.$file));
        }
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string'
        ]);
        $company = New company;
        $company->name    = $request->name;
        $company->email   = $request->email;
        $company->website = $request->website;

        if ($request->hasFile('logo')) {
            $image = $request->file('logo');
            $c = Str::slug($company->name, '_');
            $filename = $c. '_' .time() . '.' .$image->getClientOriginalExtension();
            if (!File::isDirectory('img/company/')) {
                File::makeDirectory('img/company/');
                $location = public_path('img/company/'.$filename);
                Image::make($image)->resize(400, 400)->save($location);
            }else{
                $location = public_path('img/company/'.$filename);
                Image::make($image)->resize(200, 200)->save($location);
            }
            $oldImage = $company->logo;
            $company->logo = $filename;
            if (isset($oldImage)) {
                $this->deleteImage($oldImage);
            }
        }

        $company->save();

        toastr()->success('Data Successfully Save', 'Info!');
        return redirect('/company');
    }

    public function showedit($id)
    {
        $company = company::where('id', $id)->first();
        return response()->json([
            'error' => false,
            'detail'=> $company
        ], 200);
    }

    public function update(Request $request, $id)
    {
        $data = company::findOrFail($id);
        $data->name      = $request->name;
        $data->email     = $request->email;
        $data->website   = $request->website;

        if ($request->hasFile('logo')) {
            $image = $request->file('logo');
            $c = Str::slug($data->name, '_');
            $filename = $c. '_' .time() . '.' .$image->getClientOriginalExtension();
            if (!File::isDirectory('img/company/')) {
                File::makeDirectory('img/company/');
                $location = public_path('img/company/'.$filename);
                Image::make($image)->resize(400, 400)->save($location);
            }else{
                $location = public_path('img/company/'.$filename);
                Image::make($image)->resize(200, 200)->save($location);
            }
            $oldImage = $data->logo;
            $data->logo = $filename;
            if (isset($oldImage)) {
                $this->deleteImage($oldImage);
            }
        }

        $data->update();
//        return redirect('/kontrak');

//        toastr()->success('Data Successfully Update', 'Info!');
        return response()->json([
            'success' => 'Data Successfully Update'
        ], 200);

    }

    public function showdelete($id)
    {
        $company = company::where('id', $id)->first();
        return response()->json([
            'error' => false,
            'detail'=> $company
        ], 200);
    }

    public function destroy($id)
    {
        $company = company::where('id', $id)->first();
        $company->delete();

        return response()->json([
            'success' => 'Data Successfully Deleted'
        ], 200);
    }

    public function detail($id)
    {
        $company = company::findOrFail($id);
        return view('company.detail', compact('company'));
    }
}
