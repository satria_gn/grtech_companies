<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Company;
use App\Models\Employee;
use Illuminate\Support\Facades\DB;
use App\Mail\CompanyEmail;
use Illuminate\Support\Facades\Mail;

class EmployeeController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data['company_data'] = Company::all();
        $data['employee_data'] = Employee::orderBy('id', 'DESC')->get();
        return view('employee.index', $data);
    }

    public function create()
    {
        return view('employee.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required|string',
            'last_name' => 'required|string'
        ]);
        $employee = New Employee;
        $employee->first_name          = $request->first_name;
        $employee->last_name           = $request->last_name;
        $employee->companies_id        = $request->companies_id;
        $employee->email               = $request->email;
        $employee->phone               = $request->phone;

        $aaa = Company::where('id', $request->companies_id)->first();
        Mail::to($aaa->email)->send(new CompanyEmail());

        $employee->save();

        toastr()->success('Data Successfully Save', 'Info!');
        return redirect('/employee');
    }

    public function showedit($id)
    {
        $employee = Employee::where('id', $id)->first();
        return response()->json([
            'error' => false,
            'detail'=> $employee
        ], 200);
    }

    public function update(Request $request, $id)
    {
        $employee = Employee::findOrFail($id);
        $employee->first_name         = $request->first_name;
        $employee->last_name          = $request->last_name;
        $employee->companies_id       = $request->companies_id;
        $employee->email              = $request->email;
        $employee->phone              = $request->phone;

        $employee->update();

//        toastr()->success('Data Successfully Update', 'Info!');
        return response()->json([
            'success' => 'Data Successfully Update'
        ], 200);

    }

    public function showdelete($id)
    {
        $employee = Employee::where('id', $id)->first();
        return response()->json([
            'error' => false,
            'detail'=> $employee
        ], 200);
    }

    public function destroy($id)
    {
        $employee = Employee::where('id', $id)->first();
        $employee->delete();

        return response()->json([
            'success' => 'Data Successfully Deleted'
        ], 200);
    }

    public function detail($id)
    {
        $company_data = DB::table('companies')
            ->select('companies.*')
            ->Orderby('id','DESC')
            ->get();
        $employee = Employee::findOrFail($id);

        return view('employee.detail', compact('employee','company_data'));
    }
}
